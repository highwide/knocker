class ArticleMailer < ApplicationMailer
  default to: [[ENV["TO_MAIL1"], ENV["TO_MAIL2"]]
  default from: ENV["FROM_MAIL"]
  
  def post_email(articles)
    @articles = articles
    mail subject: "条件にあう物件があったよ!!"
  end
end
