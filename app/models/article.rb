class Article
  include ActiveModel::Model

  attr_accessor \
    :site, #スクレイプ元サイト
    :name, #物件名
    :url, #詳細ページURL
    :rent, #家賃
    :management_fee, #共益費
    :deposit, #敷金
    :layout, #間取り
    :from_sta, #駅からの距離
    :address,
    :floor, #階
    :build_at, #建築年
    :option
    
end
