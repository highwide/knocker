module Scraper
  extend ActiveSupport::Concern
  require "capybara/poltergeist"

  USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36"

  def scrape_included_javascript(url)
    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, {js_errors: false, timeout: 90})
    end
    Capybara.default_selector = :xpath
    session = Capybara::Session.new(:poltergeist)

    session.driver.headers = { "User-Agent" => USER_AGENT }
    session.visit url

    page = Nokogiri::HTML.parse(session.html)
  rescue
    Rails.logger.warn("#{Time.now}: スクレイプ実行時にエラー")
  end
end
