class UrScraper
  include Scraper

  DOMAIN = "http://www.ur-net.go.jp".freeze
  PATH = "/kanto/search/tokyo_list_room.html".freeze
  PARAM = "?tcd=13&cities=109&cities=107&cities=108&rentLow=min&rentUp=120&csFee=1&walk=10&bus=&floorplan=1LDK&floorplan=2LDK&floorplan=2DK&floorplan=2K&rank=&elevator=".freeze
  SITE = "UR賃貸住宅".freeze
  EXCLUDE_ARTICLES = %w(立花一丁目)

  def main
    page = scrape_included_javascript(DOMAIN + PATH + PARAM)
    results = page.css("article.du_frame")

    if results.count > 0
      articles = results
        .map { |r| article_from_page(r) }
        .select { |a| EXCLUDE_ARTICLES.exclude? a.name }

      if articles.count > 0
        ArticleMailer.post_email(articles).deliver_now
      else
        Rails.logger.info("INFO:#{Time.now}: 条件に合う物件は除外物件しかなかったよ")
      end

    else
      Rails.logger.info("INFO:#{Time.now}: 条件に合う物件はなかったよ")
    end
  end

  private

  def article_from_page(page)
      Article.new(
        site: SITE,
        name: page.css("dt.fl a").inner_text,
        url: DOMAIN + page.css("dd.btn_base.ns_pie.btn_detail.fr a").attribute("href").value,
        rent: page.css("dl.du_states_price dd").inner_text,
        management_fee: page.css("dl.du_states_common dd").inner_text,
        deposit: page.css("dl.du_states_deposit dd").inner_text,
        layout: page.css("dl.du_states_floorplan dd").inner_text,
        from_sta: page.css("li.du_route.sp_i.ico_route")
          .map { |i| i.inner_text }
          .join(" / "),
        address: page.css("li.du_address.sp_i.ico_address").inner_text,
        floor: page.css("dl.du_floorplan.fl.cFix dd").inner_text,
        build_at: page.css("dl.du_roomsize.fl.cFix dd").inner_text.gsub(' ', ''),
        option: page.css("dl.du_equipment.cFix dd").inner_text
      )
  end
end
